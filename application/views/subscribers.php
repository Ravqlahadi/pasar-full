<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Pasarbumi</title>
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/img/favicon.ico" type="image/x-icon">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url();?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/dataTables.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url();?>assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Theme CSS -->
    <link href="<?php echo base_url();?>assets/css/agency.min.css" rel="stylesheet">
     <link href="<?php echo base_url();?>assets/css/jquery.bxslider.css" rel="stylesheet" />
    

</head>

<body id="page-top" class="index">
      <section id="tentang">
          <div class="container">
              <div class="row">
                  <div class="col-lg-12 text-center">
                      <div class="btn-group text-left">
									<?php echo anchor('/login/logout',
									'<button type="button" class="btn btn-danger
										" data-toggle="tooltip" data-placement="top" title="Keluar">
											 Logout</button>'); ?>
						</div>
					  
					  <h2 class="section-heading">Komentar Pasarbumi.id</h2>
                  </div>
              </div>
              <div class="row">
                  <div class="col-lg-12 tentang">
                    <div class='row '>
                      <div class="col-lg-9 ">
                        <div class="timeline-heading">
                          <div id="dt_example" class="example_alt_pagination" style="overflow-x:auto;">
								<br>
							  <table id="example" class="table  table-striped">

								<thead>
								  <tr>
									<th class="col-md-1">
									  No
									</th>
									<th class="">
									  Nama
									</th>
									<th class="">
									 Email
									</th>
									<th class="">
									  Pengguna
									</th>
									<th class="">
									  Komentar
									</th>
									<th class="">
									  Pilihan
									</th>
								  </tr>
								</thead>
								<tbody>
								<?php
									$no =1;
									foreach ($data as $v) {


								?>
								  <tr class="">
									<td class="col-md-1">
									  <?php echo $no ?>
									</td>
									<td class="">
									  <?php echo $v->nama ?>
									</td>
									<td class="">
									  <?php echo $v->email ?>
									</td>
									<td class="">
									  <?php echo $v->jenis_pengguna ?>
									</td>
									<td class="">
									  <?php echo $v->message ?>
									</td>
									<td class="">
									  <?php echo anchor('/komentar/delete?id='.$v->id_komentar,
										'<button type="button" class="btn btn-danger
											" data-toggle="tooltip" data-placement="top" title="Delete">
												 <i class="fa fa-trash"></i></button>'); ?>
									</td>
								  </tr>
								  <?php
										$no++;
									}
								  ?>
								</tbody>
							  </table>
							  
							</div>
                        </div>
                        <div class="timeline-body">
                          
                        </div>
                      </div>
                      <div class="col-lg-3 tentang-img">
                        <ul class="bxslider">
                          <li><img src="<?php echo base_url();?>assets/img/logo-pasarbumi.png" alt=""  class="slider-img"></li>
                          <li><img src="<?php echo base_url();?>assets/img/slider-2.png" alt=""  class="slider-img"></li>
                          <li><img src="<?php echo base_url();?>assets/img/slider-1.png" alt=""  class="slider-img"></li>
                          <li><img src="<?php echo base_url();?>assets/img/logo-pasarbumi.png" alt=""  class="slider-img"></li>
                        </ul>

                      </div>


                    </div>
                  </div>
              </div>
          </div>
      </section>

    

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <span class="copyright">Copyright &copy; Your Website 2016</span>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline social-buttons">
                        <li><a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline quicklinks">
                        <li><a href="#">Privacy Policy</a>
                        </li>
                        <li><a href="#">Terms of Use</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>


    <!-- jQuery -->
    <script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js" integrity="sha384-mE6eXfrb8jxl0rzJDBRanYqgBxtJ6Unn4/1F7q4xRRyIw7Vdg9jP4ycT7x1iVsgb" crossorigin="anonymous"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.bxslider.min.js"></script>
    <!-- Contact Form JavaScript -->
    <script src="<?php echo base_url();?>assets/js/jqBootstrapValidation.js"></script>
    <script src="<?php echo base_url();?>assets/js/contact_me.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/dataTables.bootstrap.min.js"></script>

    <!-- Theme JavaScript -->
    <script src="<?php echo base_url();?>assets/js/agency.min.js"></script>
    <!-- Script to Activate the Carousel -->
    <script>
    $(document).ready(function(){
      $('.bxslider').bxSlider();
    });
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>
	<script>
		$(document).ready(function() {
			$('#example').DataTable();
		} );
	</script>
	
</body>

</html>
