
<title>Pasarbumi</title>
<link rel="shortcut icon" href="<?php echo base_url()."assets/";?>img/favicon.ico">
<link rel="stylesheet" href="<?php echo base_url()."assets/"?>css/parallax-login.css">
<link rel="stylesheet" href="<?php echo base_url()."assets/"?>css/bootstrap.min.css">
<script src="http://mymaplist.com/js/vendor/TweenLite.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/"?>js/parallax-login.js"></script>
    <body>
        <div class="container">
            <div class="row vertical-offset-100">
                <div class="col-md-4 ">
                    <div class="panel panel-default">
                        <div class="panel-heading">                                
                            <div class="row-fluid user-row">
                                <img src="<?php echo base_url()."assets/"?>img/logo-pasarbumi.png" width="100px" height="100px" class="img-responsive" alt="Conxole Admin"/>
								
                            </div>
                        </div>
                        <div class="panel-body">
							<?php echo form_open("login/login", "id='login' class='form-signin'");?>
                            
                                <fieldset>
                                    <label class="panel-login">
                                        <div class="login_result"></div>
                                    </label>
                                    <input class="form-control" placeholder="Username" id="username" type="text" name="username">
                                    <input class="form-control" placeholder="Password" id="password" type="password" name="password">
                                    <br></br>
                                    <input class="btn btn-lg btn-success btn-block" type="submit" id="login" value="Login »">
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
				<div class="col-md-8">
					<img src="<?php echo base_url()."assets/"?>img/logo-web.png" class="img-responsive" alt="Conxole Admin"/>
					
				</div>
				
            </div>
        </div>
    </body>