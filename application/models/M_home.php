<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_home extends CI_Model{

    function __construct() {
        parent::__construct();
    }


    public function create($data) {
        //insert data
        $this->db->insert('komentar', $data);
    }
	
	public function tampil_komentar() {
        $query  = $this->db->query("select * from komentar");
        return $query->result();
    }
	
	public function delete($id) {
        $this->db->delete('komentar', array('id_komentar' => $id));
    }

    

}
?>
