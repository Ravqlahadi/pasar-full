<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Home extends CI_Controller {
    function __construct() {
        parent::__construct();
		//error_reporting(0);
		$this->load->model('m_home');
    }
	
    public function index(){
		$this->load->view('dashboard');
    }
	
	public function input() {
        //get data
        $data['id_komentar'] = "";
        $data['nama'] = $this->input->post('nama');
        $data['email'] = $this->input->post('email');
        $data['jenis_pengguna'] = $this->input->post('jenis_pengguna');
        $data['message'] = $this->input->post('message');
        $data['tanggal_message'] = date("Y-m-d");
        //call function
        $this->m_home->create($data);
        //redirect to page
        redirect('../');
    }
	
	public function subscribers(){
		$this->load->view("subscribers");
	}

    
}
