<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("m_login");
        if(!empty($_SESSION['user_id']))
            redirect('../');
    }

    public function index(){
        //$this->login_check();
        $this->load->view("login");
    }

    public function login(){
        if($_POST) {
            $data['username'] = $this->input->post('username');
            $data['password'] = md5($this->input->post('password'));
            $result = $this->m_login->login($data);
            if(!empty($result)) {
                $data = array(
                    'userid' => $result->userid,
                    'username' => $result->username,
                    //'group_id' => $result->group_id,
                    'fullname' => $result->fullname
                );
				
					$this->session->set_userdata($data);
					//log system
					//$this->m_log->create($data['userid'], "Login");
					redirect('komentar');
            } else {
                $this->session->set_flashdata('flash_data', 'Username or password is wrong!');
                redirect('login');
            }
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect(''.base_url());
    }

}
