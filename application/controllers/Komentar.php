<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//controller for modul sistem kib tanah

class Komentar extends CI_Controller {

    public function __construct(){
        parent::__construct();
		error_reporting(0);
		$this->load->model('m_home');
		//$this->load->library('upload');

		if(!($this->session->userdata('userid'))) {
            $this->session->set_flashdata('flash_data', 'Anda Tidak Mempunyai Hak Akses!');
            redirect('login');
        }

    }
	
	public function index(){
		$data["data"] = $this->m_home->tampil_komentar();
		$this->load->view('subscribers',$data);
	}
	
	public function delete() {
        if($this->input->get('id')!="") {
            $this->m_home->delete($this->input->get('id'));
        }
        //redirect to page
        redirect('komentar');

    }

    
	
	
}
