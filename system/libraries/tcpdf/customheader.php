<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Include the main TCPDF library (search for installation path).
require_once('tcpdf.php');

class CustomHeader extends TCPDF {
    /* custom table */
    // Load table data from file
    public function LoadData($file) {
        // Read file lines
        $lines = file($file);
        $data = array();
        foreach($lines as $line) {
            $data[] = explode(';', chop($line));
        }
        return $data;
    }

    // Colored table
    public function ColoredTable($header,$data) {
        // Colors, line width and bold font
        $this->SetFillColor(255, 0, 0);
        $this->SetTextColor(255);
        $this->SetDrawColor(128, 0, 0);
        $this->SetLineWidth(0.3);
        $this->SetFont('', 'B', 10);
        // Header
        //$w = array(40, 35, 100);
        
        $num_headers = count($header);
        switch($num_headers){
            case 1:{$w = array(180);break;}
            case 2:{$w = array(90,90);break;}
            case 3:{$w = array(35,120,30);break;}    
        }
        $i = 0;
        foreach($header as $row=>$value) {
            $this->Cell($w[$i], 7, $value, 1, 0, 'C', 1);
            $i++;
        }
        $this->Ln();
        // Color and font restoration
        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);
        $this->SetFont('', '', 8);
        // Data
        $fill = 0;
        $i = 0;
        $kolom = count($header);
        
        for($i = 0; $i < count($data); $i++) {
            $c = 0;
            foreach($data[$i] as $row=>$key){
                
                //format tanggal ditengah
                if($row == "tanggal"){
                    $this->Cell($w[$c], 6,$key, 'LR', 0,'C',$fill);
                } else {
                    //format jumlah uang dikanan
                    if($row == "pagu"){
                        $this->Cell($w[$c], 6,$key, 'LR', 0, 'R',$fill);    
                    } else {
                        $this->Cell($w[$c], 6,$key, 'LR', 0, 'L',$fill);
                    }    
                } 
                $c++;
            }
            $this->Ln();
               $fill=!$fill;         
        }
        $this->Cell(array_sum($w), 0, '', 'T');
    }
    
    //Page header
    public function Header() {
        // Logo
        $image_file = K_PATH_IMAGES.'logos.jpg';
        $this->Image($image_file, 21, 10, 25, 25, 'JPG', '', 'T', false, 500, '', false, false, 0, false, false, false);
        
        // Set font
        $this->SetFont('helvetica', 'B', 17);
        // Title 
        $this->Cell(0, 15, 'PEMERINTAH KOTA KENDARI', 0, 1, 'C', 0, '', 0, false, 'M', 'M');
        $this->SetFont('helvetica', 'B', 14);
        $this->Cell(210, 12, 'BAG. ADMINISTRASI PEMBANGUNAN & PENGADAAN' , 0, 3, 'C', 0, '', 0, false, 'M', 'M');
        
        $this->SetFont('helvetica', 'B', 14);
        $this->Cell(210, 12, 'SETDA KOTA KENDARI' , 0, 3, 'C', 0, '', 0, false, 'M', 'M');
        
        $this->SetFont('helvetica', 'I', 10);
        $this->Cell(210, 20, 'Jalan Drs. H. Abdullah Silondae No. 8 telp. (0401) 321402 Fax. 323593 Kendari', 0, 3, 'C', 0, '', 0, false, 'M', 'M');
        
        $style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0));
        $this->Line(12, 40, 200, 40, $style);
        
    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Halaman '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
    
}